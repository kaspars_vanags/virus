@extends("layouts.auth")

@section("body")
    <a href="/logout">Logout bitches</a>

    <h1>Node admin</h1>
    <div class="row justify-content-center">
        <div class="col col-8">
            <button class="btn btn-dark" id="button-get-connection-list">Get Connection List</button>
            <button class="btn btn-dark" id="button-get-all-rooms">Get All Rooms</button>
            <button class="btn btn-dark" id="button-get-room-status">Get Room Status</button>
            <button class="btn btn-dark" id="button-get-user-fields">Get User Fields</button>
            <h3>Game rooms</h3>
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>Room ID</th>
                    <th>Owner</th>
                    <th>Joined</th>
                    <th>Status</th>
                    <th>Created At</th>
                    <th>Type</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($roomList as $room)
                        <tr>
                            <td>{{ $room['game_room_id'] }}</td>
                            <td>{{ $room['owner_id'] }}</td>
                            <td>{{ $room['joined_id'] }}</td>
                            <td>{{ $room['room_status'] }}</td>
                            <td>{{ $room['room_created_date'] }}</td>
                            <td>{{ $room['game_type'] }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script src="/js/socket.io/socket.io.js"></script>

    <script type="text/javascript">
        var socket = io("http://157.245.39.160:3000");

        socket.on("adminConnectionListResponse", function(data) {
            console.log(data);
        });

        socket.on("adminRoomListResponse", function(data) {
            console.log(data);
        });

        socket.on("adminRoomStatusResponse", function(data) {
            console.log(data);
        });

        socket.on("adminUserFieldsResponse", function(data) {
            console.log(data);
        });


        $(document).ready(function () {

            $("#button-get-connection-list").on("click", function(){
                socket.emit("adminGetConnectedList");
            });

            $("#button-get-all-rooms").on("click", function(){
                socket.emit("adminGetRoomList");
            });

            $("#button-get-room-status").on("click", function(){
                let roomName = prompt("Enter room name: ", "");

                if (roomName == "") {
                    alert("Empty value entered");
                    return;
                }

                socket.emit("adminGetRoomStatus", {"roomName" : roomName});
            });

            $("#button-get-user-fields").on("click", function(){
                let playerId = prompt("Enter player id: ", "");

                if (playerId == "") {
                    alert("Empty value entered");
                    return;
                }

                let roomId = prompt("Enter room id:", "");

                if (roomId == "") {
                    alert("Empty value entered");
                    return;
                }

                socket.emit("adminGetUserFields", {"playerId": playerId, "roomId" : roomId});
            });

        });
    </script>
@endsection
