@extends("layouts.auth")

@section("body")
    <a href="/logout">Logout bitches</a>

    <h1>Hi this is admin panel</h1>
    <div class="row justify-content-center">
        <div class="col col-8">
            <h3>Open game rooms</h3>
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <th>Room ID</th>
                        <th>Owner</th>
                        <th>Joined</th>
                        <th>Status</th>
                        <th>Created At</th>
                        <th>Type</th>
                        <th>Close</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($response["rooms"] as $room)
                        <tr>
                            <td>{{ $room['game_room_id'] }}</td>
                            <td>{{ $room['owner_id'] }}</td>
                            <td>{{ $room['joined_id'] }}</td>
                            <td>{{ $room['room_status'] }}</td>
                            <td>{{ $room['room_created_date'] }}</td>
                            <td>{{ $room['game_type'] }}</td>
                            <td><a href="/admin/room/close/{{$room['game_room_id']}}" class="btn btn-warning">close</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
<!--
    $table->bigIncrements('game_room_id');
    $table->unsignedInteger("owner_id");
    $table->unsignedInteger("joined_id")->nullable();
    //open - people can join | connected - users are connected | ready - users are playing | complete - game has ended
    $table->string("room_status")->default("open");
    $table->dateTime("room_created_date")->nullable(); //time room was created //if status in 1 h isn't ready it will be closed
    $table->dateTime("room_started_date")->nullable(); //time when game actually started and closed if users hasn't completed in 1 h
    $table->unsignedInteger("game_type")->default(1);
    -->

@endsection
