@extends("layouts.auth")

@section("body")
    <div class="row justify-content-center">
        <div class="col col-4">
            <!--<h4>This is game room for {{$response["roomName"]}}</h4>-->
        </div>
    </div>

    @if($response["gameType"] == 1)
        @include("auth.games.1vs1")
    @endif
    @if($response["gameType"] == 2)
        @include("auth.games.1vspc")
    @endif
    @if($response["gameType"] == 3)
        @include("auth.games.2vs2")
    @endif


@endsection

