

    <div id="game-notification-holder">
        <h4>You got hit!</h4>
    </div>

    <div class="row justify-content-center" style="margin-top: 20px">
        <div class="col-3">
            <div class="card" style="background-color: #E0E0E0;">
                <div class="card-header">
                    <h4>Enemy virus left: <span id="enemy-virus-count">10</span></h4>
                </div>
                <div class="card-body">
                    <p>Available viruses</p>
                    <ul style="padding-left: 0; list-style: none">
                        <li>
                            <button class="btn btn-warning btn-virus-selector" data-virustype="2">
                                Select virus
                                <p style="margin-bottom: 0">
                                2 Squares
                                </p>
                            </button>
                        </li>
                        <li class="virus-left" id="virus-left-2">4 / 4</li>
                        <li>
                            <button class="btn btn-warning btn-virus-selector" data-virustype="3">
                                Select virus
                                <p style="margin-bottom: 0">
                                    3 Squares
                                </p>
                            </button>
                        </li>
                        <li class="virus-left" id="virus-left-3">3 / 3</li>
                        <li><button class="btn btn-warning btn-virus-selector" data-virustype="4">
                                Select virus
                                <p style="margin-bottom: 0">
                                    4 Squares
                                </p>
                            </button>
                        </li>
                        <li class="virus-left" id="virus-left-4"> 2 / 2 </li>

                        <li>
                            <button class="btn btn-warning btn-virus-selector" data-virustype="5">
                                Select virus
                                <p style="margin-bottom: 0">
                                    5 Squares
                                </p>
                            </button>
                        </li>
                        <li class="virus-left" id="virus-left-5"> 1 / 1</li>

                    </ul>
                </div>
            </div>
        </div>
        <div class="col-7">
            <div id="result-holder" class="result-overflow">
                <h4>Victory</h4>
            </div>
            <div class="card" style=" background-color: #E0E0E0;">
                <div class="card-header" style="padding-left: 0; padding-right: 0; padding-bottom: 0">
                    <button class="btn btn-warning btn-start-game visible" id="start-game-button">Start game</button>
                    <button class="btn btn-warning btn-warning invisible" id="give-up-game-button">Give up!</button>

                    <div id="card-timer" data-status="" data-timer="" class="card-footer card-timer-bar">
                    </div>
                </div>
                <div class="card-body">
                    <div class="row justify-content-center">
                        <!-- draw first line as its always numbers -->
                            <div class="game-name-fields top name-col-azk">
                                <!-- empty one -->
                            </div>
                        @for($i = 1; $i <= 10; $i++)
                            <div class="game-name-fields top horizonta-col-{{$i}}">
                                <p class="text-center">{{ $i }}</p>
                            </div>
                        @endfor
                    </div>

                    @for($i = 1; $i <= 10; $i++)
                    <div class="row justify-content-center">
                        <div class="game-name-fields side vertical-naming-cell">
                            <p>{{ $data["verticalNaming"][$i-1] }}</p>
                        </div>
                        @for($j = 1; $j <= 10; $j++)
                            <div class="game-field-item" id="game-field-item-v{{$i}}-h{{$j}}" data-hasbeenhit="0" data-enemystatus="0" data-locV="{{ $i }}" data-locH="{{ $j }}" data-hasvirus="0" style="border: 1px solid #212121;">
                            </div>
                        @endfor
                    </div>
                    @endfor
                </div>
                <div class="card-footer">
                    <!--
                    <h4>action buttons</h4>
                    <button class="btn btn-warning" id="attack-button">Attack</button>
                    <h4>action buttons</h4>
                    <button class="btn btn-warning" id="defence-button">Defence</button>
                    -->
                </div>
            </div>
        </div>
    </div>

    @include("auth.widgets.messageBoard")


    @section("script")
        <script>
            var myUserId = {{ Auth::user()->user_id  }};
            var roomName = "{{$response["roomName"]}}";
            var roomId = {{ $response["roomId"] }};
            var roomType = "onevsone";
        </script>

        <script src="{{ URL::asset("/js/socket.io/socket.io.js") }}"></script>
        <script src="{{ URL::asset("js/game-field.js") }}" type="text/javascript"></script>
        <script src="{{ URL::asset("js/game-1vs1.js") }}" type="text/javascript"></script>
    @endsection
