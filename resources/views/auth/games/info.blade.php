@extends("layouts.auth")

@section("body")

    <div class="info-wrapper" style="margin-top: 50px;">
        <div class="row justify-content-center">
            <div class="col col-6">
                <h3 style="text-transform: uppercase; font-weight: 900; text-align: right;">Winner!</h3>
                <h1 style="font-weight: 900;color: #fff; font-size: 2.75rem; text-align: right;">{{ $info["winnerName"] }}</h1>

                <h4 style="text-transform: uppercase; font-weight: 900; text-align: right;">2ND Place!</h4>
                <h5 style="font-weight: 900;color: #fff; text-align: right;">{{ $info["looserName"] }}</h5>
            </div>
            <div class="col col-6" style="">
                <h3 style="font-weight: 900;">Game took </h3>
                <h1 style="font-weight: 900;color: #fff; font-size: 2.75rem; ">{{ $info["timePlayed"] }} MIN</h1>
                <h3 style="font-weight: 900;">to be completed!</h3>
            </div>
        </div>
        <div class="row justify-content-center" style="margin-top: 35px">
            <a href="/game" class="btn btn-navigation">Back to games</a>
        </div>
    </div>

@endsection
