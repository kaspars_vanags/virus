@extends("layouts.auth")

@section("body")

    <div class="profile-wrapper">
        <div class="row justify-content-center">
            <div class="col col-6">
                <div class="profile-banner-container">
                    <h1>{{ Auth::user()->username }}</h1>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col col-2">
                <div class="profile-menu-box row">
                    <div class="profile-menu-box__icon col col-3">
                        <i class="fas fa-user-cog"></i>
                    </div>
                    <div class="profile-menu-box__info col col-9">
                        <h4>Profile</h4>
                        <p>All your profile settings</p>
                    </div>
                </div>
            </div>
            <div class="col col-2">
                <div class="profile-menu-box row">
                    <div class="profile-menu-box__icon col-3">
                        <i class="fas fa-award"></i>
                    </div>
                    <div class="profile-menu-box__info col-9">
                        <h4>Achievements</h4>
                        <p>What you done...</p>
                    </div>
                </div>
            </div>
            <div class="col col-2">
                <div class="profile-menu-box row">
                    <div class="profile-menu-box__icon col-3">
                        <i class="fas fa-question"></i>
                    </div>
                    <div class="profile-menu-box__info col-9">
                        <h4>Other</h4>
                        <p>Some other stuff</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col col-3">
                <div class="profile-last-games-box ">
                    <h4>Last 20 Games</h4>
                    <hr>
                    <div class="row">
                        <div class="col col-2"><p>(Against)</p></div>
                        <div class="col col-2"><p>(When)</p></div>
                        <div class="col col-2"><p>(Status)</p></div>
                    </div>
                    @foreach($data["game_history"] as $game)
                        <div class="row @if($game["winner"]) winner @else looser @endif">
                            <div class="col col-2"><p>{{ $game["enemy"] }}</p></div>
                            <div class="col col-2"><p>{{ $game["when"] }}</p></div>
                            <div class="col col-2"><p>{{ $game["status"] }}</p></div>
                            @if($game["status"] != "complete")
                                <div class="col col-3">
                                    <button class="btn btn-success">Re-connect</button>
                                </div>
                                <div class="col col-2">
                                    <button class="btn btn-warning">Close</button>
                                </div>
                            @else
                                <div class="col col-2">
                                    <p>Victory</p>
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col col-1">
            </div>
            <div class="col col-2">
                <div class="profile-last-games-box ">
                    <h4>my last games</h4>
                </div>
            </div>
        </div>
    </div>

@endsection
