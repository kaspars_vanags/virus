@extends("layouts.auth")

@section("body")
    <div class="row justify-content-center">
        <div class="col col-5">
            <div class="card">
                <div class="card-header">
                    <h4>Games available</h4>
                </div>
                <div class="card-body">
                    <div class="card game-list-card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col col-4">
                                    <p class="text-center">1 VS 1</p>
                                </div>
                                <div class="col-8">
                                    <a id="game-button_onevsone" href="#" data-buttonstate="play" data-gametype="onevsone" class="btn btn-success float-right game-list-card__button">Play</a>
                                </div>
                            </div>
                        </div>
                        <div id="card-footer-onevsone" class="card-footer card-progress-bar">
                        </div>
                    </div>
                    <div class="card game-list-card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col col-4">
                                    <p class="text-center">1 VS COM</p>
                                </div>
                                <div class="col-8">
                                    <a href="/game/onevscom" data-gametype="onevscom" class="btn btn-success float-right game-list-card__button">Play</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer card-progress-bar card-footer-1vscom">
                        </div>
                    </div>
                    <div class="card game-list-card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col col-4">
                                    <p class="text-center">2 VS 2</p>
                                </div>
                                <div class="col-8">
                                    <a href="/game/twovstwo" data-gametype="twovstwo" class="btn btn-success float-right game-list-card__button">Play</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer card-progress-bar card-footer-2vs2">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalForRoomConfirmation">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Room has been found</h2>
                </div>
                <div class="modal-body">
                    <p>You will be redirected to the room just in a sec [put timer here]</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")

    <script src="/js/socket.io/socket.io.js"></script>

    <!-- game/onevssone -->
    <script type="text/javascript">

    var gameInProgress = {"onevsone" : {{$gamesAvailable["1vs1"]}}, "onevscom" : {{$gamesAvailable["1vsPc"]}}, "twovstwo"  : {{$gamesAvailable["2vs2"]}} };
    var userId = "also from db";
    var gameType = "";

    var socket = io('http://157.245.39.160:3000');


    //Socket Returns notification that connection has been created and we can pass user details
    socket.on("connectionEstablished", function(data){
        if(data.status == 1) {
            socket.emit("addUserToConnectionList", {userId : {{ Auth::user()->user_id  }} });
        }
    });

    /*
    socket.on('createTheRoom', function(data){
        console.log("[ON] createTheRoom with "+data.roomType);
        createGameRoom(data.roomType)
    });
    */
    /*
    socket.on('roomAddedToList', function(data) {
        alert(data.message);
        console.log(data);
    });
*/

    socket.on('foundRoomAndConnecting', function(data) {
        console.log("Found room with ID [ "+data.id+" ]");
        //show modal with message that room has been found
        //TODO: edit info on modal
        $("#modalForRoomConfirmation").show();

        //2 put time out and redirect to room id
        setTimeout(function() {
            window.location.replace("/game/active/"+data.id+"/"+data.name);
        }, 5000)
    });

    socket.on("connectionFound", function (data) {
        //room id so need rederect
        alert("Someone connected to your room");

        setTimeout(function() {
            window.location.replace("/game/active/"+data.roomId+"/"+data.roomName);
        },5000)

    });

    socket.on("searchHasBeenCanceled", function(data){
        console.log("[ON] searchHasBeenCanceled with "+data);
        //removeGameRoom(data);

        changeButtonState(data["roomType"], "Cancel");
        removeLoader(data["roomType"]);
    });

    $(".game-list-card__button").on("click", function() {

        //get game type
        let gameType = $(this).data("gametype");
        let gameState = $(this).data("buttonstate");

        if(gameState.toLowerCase() == "cancel") {
            console.log("Call [cancelGameSearch] function with userId = {{ Auth::user()->user_id  }}");
            socket.emit("cancelGameSearch", {userId: {{ Auth::user()->user_id }}, roomType: gameType});
            return;
        }

        //1 check if gameInProgress true (info from back-end), if so, need to connect to existing game
        if(gameInProgress[gameType] !== 0) {
            console.log("Call [reconnectToTheGame] function with roomId = "+gameInProgress[gameType]);
            socket.emit("reconnectToTheGame", {roomId: gameInProgress[gameType]});
            return; //because i need exit and not search for new game room
        }

        addLoader(gameType);
        changeButtonState(gameType, gameState);

        //check if there is no room available
        socket.emit("findOrCreateGameRoom", {roomType: gameType, userId : {{Auth::user()->user_id}} });
    });

    function addLoader(gameType) {
        let footerItem = $("#card-footer-"+gameType);
        footerItem.addClass("show");
    }

    function removeLoader(gameType) {
        let footerItem = $("#card-footer-"+gameType);
        footerItem.removeClass("show");
    }

    function changeButtonState(gameType, currentState) {
        let newState;
        let buttonHolder = $("#game-button_"+gameType);

        if(currentState.toLowerCase() == "play") {
            newState = "Cancel";
            buttonHolder.removeClass("btn-success");
            buttonHolder.addClass("btn-warning");
        } else {
            newState = "Play";
            buttonHolder.removeClass("btn-warning");
            buttonHolder.addClass("btn-success");
        }

        buttonHolder.data("buttonstate", newState);
        buttonHolder.html(newState);

    }
/*
    function createGameRoom(gameType) {
        console.log('Just before hitting CREATE ROOM WITH GAME TYPE '+gameType);
        jQuery.get("/game/create/gameroom", {"type": gameType}).done(function(r) {
            console.log("Respomse", r);

            if(r["status"]) {
                console.log("created room with ID: "+r['roomId']);
                socket.emit("addRoomToLine", {"roomId" : r["roomId"], "roomType" : r["gameType"], "roomName" : r["roomName"], "ownerId" : {{ Auth::user()->user_id}} });
            }
        });

    }
*/
/*
    function removeGameRoom(data) {
        changeButtonState(data["roomType"], "Cancel");
        removeLoader(data["roomType"]);

        console.log('Closing game search '+data["roomType"]);
        jQuery.get("/game/close/gameroom", {"roomId": data["id"]}).done(function(r) {
            console.log("Response", r);

            if(r["status"]) {
                changeButtonState(data["roomType"], "Cancel");
                removeLoader(data["roomType"]);
                //console.log("created room with ID: "+r['roomId']);
                //socket.emit("addRoomToLine", {"roomId" : r["roomId"], "roomType" : r["gameType"], "roomName" : r["roomName"], "ownerId" : {{ Auth::user()->user_id}} });
            }
        });
    }
    */


    </script>

@endsection
