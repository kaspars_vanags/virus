<div class="navbar navbar-expand-lg navbar-light ">
    <button class="navbar-toggler" data-toggle="collapse" data-target="guestNavbar" data-arie="guestNavbar">
        <div class="navbar-toggler-icon">
        </div>
    </button>
    <div class="navbar navbar-collapse">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="/" class="nav-link">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link">Gallery</a>
            </li>
            <li class="nav-item">
                <a class="nav-link">Instructions</a>
            </li>
        </ul>
        <div>
            <a href="/login" class="nav-link">Login</a>
        </div>
    </div>
</div>
