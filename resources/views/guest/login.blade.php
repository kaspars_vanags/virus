<?php
/**
 * Created by PhpStorm.
 * User: Kaspars
 * Date: 8/25/2019
 * Time: 15:29
 */


?>

@extends("layouts.welcome")

@section("body")
    <div class="row justify-content-center">
        <div class="col col-4">
            <h4>Login</h4>
            <form method="POST" action="/login" class="form">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="username" class="control-label">Username</label>
                    <input type="text" class="form-control" id="username" name="username" placeholder="Enter username...">
                </div>
                <div class="form-group">
                    <label for="password" class="control-label">Password</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
                <div class="form-group">
                    <button class="btn btn-dark">Login</button>
                </div>
            </form>
        </div>
        <div class="col col-4">
            <h4>Register</h4>
            <form method="POST" action="/register" class="form">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="reg-username" class="control-label">Username</label>
                    <input type="text" class="form-control" id="reg-username" name="reg-username" placeholder="Enter username...">
                </div>
                <div class="form-group">
                    <label for="reg-email" class="control-label">Email</label>
                    <input type="email" class="form-control" id="reg-email" name="reg-email" placeholder="Email address for email verification">
                </div>
                <div class="form-group">
                    <label for="reg-password" class="control-label">Password</label>
                    <input type="password" class="form-control" id="reg-password" name="reg-password">
                </div>
                <div class="form-group">
                    <label for="reg-re-password" class="control-label">Repeat Password</label>
                    <input type="password" class="form-control" id="reg-re-password" name="reg-re-password">
                </div>
                <div class="form-group">
                    <button class="btn btn-dark">Register</button>
                </div>
            </form>
        </div>
    </div>
@endsection
