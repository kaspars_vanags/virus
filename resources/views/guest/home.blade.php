<?php
/**
 * Created by PhpStorm.
 * User: Kaspars
 * Date: 8/25/2019
 * Time: 14:29
 */
$count = 1

?>

@extends("layouts.welcome")

@section("body")
    <div class="info-wrapper" style="margin-top: 50px;">
        <div class="row justify-content-center">
            <div class="col col-4">
                <h3 style="text-transform: uppercase; font-weight: 900; text-align: left;">Leader Board!</h3>
                <h1 style="font-weight: 900;color: #fff; font-size: 2.75rem; text-align: left;">1 vs 1</h1>
            </div>
            <div class="col col-4" style="">
                <h3 style="text-transform: uppercase; font-weight: 900; text-align: left;">Leader Board!</h3>
                <h1 style="font-weight: 900;color: #fff; font-size: 2.75rem; text-align: left;">2 VS 2</h1>
            </div>
        </div>
        <div class="row justify-content-center" style="margin-top: 35px">
            <div class="col col-4">
                <table>
                    <thead>
                        <tr style="border-bottom: 1px solid #212121">
                            <th style="padding-right: 10px; border-right: 1px solid #212121;">Place</th>
                            <th style="padding-left: 10px;">User</th>
                            <th>Wins</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($leaderBoard["1vs1"] as $leader)
                            <tr>
                                <td style="border-right: 1px solid #212121;">{{ $count++ }}</td>
                                <td style="padding-right: 25px; padding-left: 10px;">{{ $leader["username"] }}</td>
                                <td>{{ $leader["wins"] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col col-4">
                <table>
                    <thead>
                    <tr style="border-bottom: 1px solid #212121">
                        <th style="padding-right: 10px; border-right: 1px solid #212121;">Place</th>
                        <th style="padding-left: 10px;">User</th>
                        <th>Wins</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($leaderBoard["2vs2"] as $leader)
                        <tr>
                            <td style="border-right: 1px solid #212121;">{{ $count++ }}</td>
                            <td style="padding-right: 25px; padding-left: 10px;">{{ $leader["username"] }}</td>
                            <td>{{ $leader["wins"] }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--
    <div class="row">
        <div id="login-container" class="col col-5">
            <form class="form">
                <div class="form-group">
                    <label for="">Username</label>
                    <input type="text" class="form-control" id="username">
                </div>
                <div class="form-group">
                    <label></label>
                    <input>
                </div>
            </form>
        </div>
        <a href="/login">Login / Register</a>
    </div>
    -->
@endsection
