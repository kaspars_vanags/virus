
var socket = io("http://157.245.39.160:3000");//io("http://138.68.159.121:3000");

//Socket Returns notification that connection has been created and we can pass user details
socket.on("connectionEstablished", function(data){
    console.log("[connectionEstablished] Connection established");
    if(data.status == 1) {
        socket.emit("addUserToConnectionList", {userId : myUserId, connectedToRoom: true, "roomIdentifier": roomName });
    }
});


board = {
    self: this,
    socket: "",
    gameData: {
        "properties" : {
            "hoverColour": "#AED581",//"rgba(100,181,246 ,0.75)",
            "errorColour": "#FF7043",//"rgba(240,98,146 ,0.75)",
            "virusColour": "#8BC34A",//"rgba(67,160,71 ,1)",
            "boardColour": "#90A4AE",//"#fff",
            //colors for attack mode
            "fieldEmpty" : "#263238",//"rgba(79,195,247 ,0.75)",
            "fieldAttacked" : "#D81B60",//rgba(216,27,96 ,0.75)",
            "fieldSinked" : "#d50000",//"rgba(96,125,139 ,0.75)",
            "attackHoverColor" : "#e57373",//"rgba(183,28,28 ,0.80)",
            //defence mode
            "fieldHasBeenSeenByEnemy": "#263238",//"rgba(79,195,247 ,0.75)",
            "fieldHasBeenHitByEnemy": "#D81B60",//"rgba(216,27,96 ,0.75)",
        },
        "virus" : {
            "types" : {
                "usedTwo"  : 0,
                "usedThree": 0,
                "usedFour" : 0,
                "usedFive" : 0
            },
            "selectedVirusType" : 0,
            "selectedVirusTimeoutId": 0,
            "rotation": false,
            "enemyVirusLeft": 10,
        },
        "player" : {
            "name" : ""
            //other info
        },
        "board" : {
            "fieldMode" : "placement", //placement/attack/defence/ready
            "attackTimer": 40
            //don't know yet
        }
    },
    init: function(id){
        board.gameData.virus.selectedVirusTimeoutId = id;
   //     board.self = ;
    },
    virusApi : {
        selectVirus: function(el) {

            var virusType = el.data("virustype");
            board.gameData.virus.selectedVirusType = virusType;

            if(board.virusApi.checkAvailableVirus(virusType)) {
                board.boardApi.showMessage("Invalid virus", "danger", "<h4>You have already placed all your viruses of this type</h4>");
                return;
            }

            var html = "";

            for(var i = 0; i < virusType; i++) {
                html += '<div class="virus-box col col-1"></div>';
            }

            board.boardApi.showMessage("selected virus", "info", html);
        },
        rotateVirus: function(el) {
            console.log(this);
            //if its any size virus you could rotate it verticle or horizontal
        },
        placeVirus: function(locationH, locationV) {

            if(board.gameData.board.fieldMode != "placement"){
                return;
            }
            //TODO: check if it doesn't overlap other itesm

            //Place it
            let dimension = (board.gameData.virus.rotation) ? locationV : locationH;
            var spaceAllocated = dimension + board.gameData.virus.selectedVirusType;
            var placedItemColour =  board.gameData.properties.virusColour; //"rgba(67,160,71 ,1)";

            if(spaceAllocated > 11) {
                return;
            }

            if(board.virusApi.checkAvailableVirus(board.gameData.virus.selectedVirusType)) {
                board.boardApi.showMessage("Invalid virus", "danger", "<h4>You have already placed all your viruses of this type</h4>");
                return;
            }

            var touchVirus = false;

            //make first run to check if touches any other virus
            for(var i = dimension; i < spaceAllocated; i++) {

                if(board.gameData.virus.rotation) {
                    if(board.virusApi.checkTakenFields(locationH, i)) {
                        touchVirus = true;
                    }
                } else {
                    if(board.virusApi.checkTakenFields(i, locationV)) {
                        touchVirus = true;
                    }
                }

            }

            if(touchVirus) {
                return;
            }

            for(var i = dimension; i < spaceAllocated; i++) {
                let locH = (board.gameData.virus.rotation) ? locationH : i;
                let locV = (board.gameData.virus.rotation) ? i : locationV;

                $("#game-field-item-v"+locV+"-h"+locH).data("hasvirus", 1);
                $("#game-field-item-v"+locV+"-h"+locH).css("background", placedItemColour);
            }

            let msg = "";
            //add counter as used
            if(board.gameData.virus.selectedVirusType == 2) {
                board.gameData.virus.types.usedTwo++;
                msg = (4 - board.gameData.virus.types.usedTwo)+" / 4";
            }

            if(board.gameData.virus.selectedVirusType == 3) {
                board.gameData.virus.types.usedThree++;
                msg = (3 - board.gameData.virus.types.usedThree)+" / 3";
            }

            if(board.gameData.virus.selectedVirusType == 4) {
                board.gameData.virus.types.usedFour++;
                msg = (2 - board.gameData.virus.types.usedFour)+" / 2";
            }

            if(board.gameData.virus.selectedVirusType == 5) {
                board.gameData.virus.types.usedFive++;
                msg = (1 - board.gameData.virus.types.usedFive)+" / 1";
            }

            $("#virus-left-"+board.gameData.virus.selectedVirusType).html(msg);

            return;
            //place virus on board
        },
        attackVirus: function(locationH, locationV) {

            if(board.gameData.board.fieldMode == "placement"){
                return;
            }

            board.socketApi.attackVirus(locationH, locationV);
            board.boardApi.hideAttackTimer();

            //Todo: all this should be made with emiting

            //check if field is not attacked

            //if its whole virus, draw around virus 3

            //if just hit make it as hit


            //Enemy status
            //0 field not attacked
            //1 field attacked but no enemy
            //2 field attacked enemy found
            //3 field empty because user killed enemy virus

        },
        removeVirus: function (el) {
            //remove virus from board and reset it as used
        },
        openVirusMenu: function () {
            //if virus is placed you could open menu, what to do with virus
        },
        moveVirus: function(locationH, locationV) {
            //this should show outlines on board when you move around board

            //check if its fit and set color for it
            let dimension = (board.gameData.virus.rotation) ? locationV : locationH;
            let spaceAllocated = dimension + board.gameData.virus.selectedVirusType;
            let hoverColour = board.gameData.properties.hoverColour;
//            var untilWhenColour = spaceAllocated;

            if(spaceAllocated > 11) {
                hoverColour = board.gameData.properties.errorColour;
                spaceAllocated = 11;
            }

            var hoveredItem = "";

            if(!board.gameData.virus.rotation) {
                for (var i = locationH; i < spaceAllocated; i++) {
                    hoveredItem = $("#game-field-item-v" + locationV + "-h" + i);

                    if (hoveredItem.data("hasvirus") == 1) {
                        $("#game-field-item-v" + locationV + "-h" + i).css("background", board.gameData.properties.errorColour);
                    } else {
                        $("#game-field-item-v" + locationV + "-h" + i).css("background", hoverColour);
                    }

                }
            } else {
                for (var i = locationV; i < spaceAllocated; i++) {
                    hoveredItem = $("#game-field-item-v" + i + "-h" + locationH);

                    if (hoveredItem.data("hasvirus") == 1) {
                        $("#game-field-item-v" + i + "-h" + locationH).css("background", board.gameData.properties.errorColour);
                    } else {
                        $("#game-field-item-v" + i + "-h" + locationH).css("background", hoverColour);
                    }

                }
            }


        },
        moveAttacker: function(locationH, locationV) {
            var hoverColour = board.gameData.properties.attackHoverColor;
            var hoveredItem = $("#game-field-item-v"+locationV+"-h"+locationH);

            //TODO: add check if its already sinked etc

            hoveredItem.css("background", hoverColour);
        },
        checkAvailableVirus: function(number) {

            var returnValue = false;

            switch (number) {
                case 2:
                    if(board.gameData.virus.types.usedTwo >= 4) {
                        returnValue = true;
                    }
                    break;
                case 3:
                    if(board.gameData.virus.types.usedThree >= 3) {
                        returnValue = true;
                    }
                    break;
                case 4:
                    if(board.gameData.virus.types.usedFour >= 2) {
                        returnValue = true;
                    }
                    break;
                case 5:
                    if(board.gameData.virus.types.usedFive >= 1) {
                        returnValue = true;
                    }
                    break;
            }

            return returnValue;
        },
        checkTakenFields: function(locH, locV) {

            var currentItem, itemTop, itemBottom, itemRight, itemLeft,
                topCornerLeft, bottomCornerLeft, topCornerRight, bottomCornerRight;

            currentItem = $("#game-field-item-v"+locV+"-h"+locH);
            if(currentItem.data("hasvirus")) {
                return true;
            }

            itemTop = $("#game-field-item-v"+(locV+1)+"-h"+locH);
            if(itemTop.data("hasvirus")) {
                return true;
            }

            itemBottom = $("#game-field-item-v"+(locV-1)+"-h"+locH);
            if(itemBottom.data("hasvirus")) {
                return true;
            }

            itemRight = $("#game-field-item-v"+locV+"-h"+(locH+1));
            if(itemRight.data("hasvirus")) {
                return true;
            }

            itemLeft = $("#game-field-item-v"+locV+"-h"+(locH-1));
            if(itemLeft.data("hasvirus")) {
                return true;
            }

            //check corners if they have virus
            topCornerRight = $("#game-field-item-v"+(locV+1)+"-h"+(locH+1));
            if(topCornerRight.data("hasvirus")) {
                return true;
            };

            bottomCornerRight = $("#game-field-item-v"+(locV-1)+"-h"+(locH+1));
            if(bottomCornerRight.data("hasvirus")) {
                return true;
            };

            topCornerLeft = $("#game-field-item-v"+(locV+1)+"-h"+(locH-1));
            if(topCornerLeft.data("hasvirus")) {
                return true;
            };

            bottomCornerLeft = $("#game-field-item-v"+(locV-1)+"-h"+(locH-1));
            if(bottomCornerLeft.data("hasvirus")) {
                return true;
            };

            return false;
        }
    },
    playerApi : {
        //maybe something like send messages
        //receive messages
        //open vault etc
    },
    socketApi: {
        playerReady: function() {
            console.log("Collecting board field info");
            let fieldList = board.boardApi.getBoardFields();

            console.log("Emit playeReady function with params", {"playerId": myUserId, "roomName": roomName, "fieldList": fieldList, "roomId": roomId});
            /*console.log(board.socket.emit("playerReady", {"playerId": myUserId, "roomName": roomName, "fieldList": fieldList, "roomId": roomId}));*/

            board.socket.emit("playerReady", {"roomType": roomType, "playerId": myUserId, "roomName": roomName, "fieldList": fieldList, "roomId": roomId});
        },
        attackVirus: function(h, v) {
            board.socket.emit("attackVirus", {"playerId": myUserId, "roomName": roomName, "roomId": roomId, "locH": h, "locV": v})
        },
        changePlayerState: function(state) {
            console.log("Change player state to: "+state);
            board.socket.emit("changePlayerState", {"playerId": myUserId, "roomId": roomId, "state": state, "roomName": roomName});
        },
        emitGiveUp: function() {
            console.log("Emit [emitGiveUp] for player "+ myUserId);
            board.socket.emit("giveUpGame", {"playerId": myUserId, "roomName": roomName,  "roomId": roomId})
        }
    },
    notificationApi: {
        changeEnemyVirusCount: function () {
            board.gameData.virus.enemyVirusLeft = board.gameData.virus.enemyVirusLeft - 1;
            $("#enemy-virus-count").html(board.gameData.virus.enemyVirusLeft);
        },
        showInGameNotification: function(msg, status) {
            let messageHolder = $("#game-notification-holder");

            messageHolder.find("h4").html(msg);
            messageHolder.addClass(status);

            setTimeout(board.notificationApi.hideInGameNotification, 1000);
        },
        hideInGameNotification: function () {
            let messageHolder = $("#game-notification-holder");
            messageHolder.removeClass("positive");
            messageHolder.removeClass("negative");
        }
    },
    boardApi : {
        getBoardFields: function() {
            let allItems = $(".game-field-item");
            let fieldList = [];

            //TODO Add last validation does fields not overflow each other
            $.each(allItems, function(e,i){
                //console.log($(i).data("hasvirus"))
                //if($(i).data("hasvirus") == 1) {
                let hasVirus = $(i).data("hasvirus");
                let enemyStatus = $(i).data("enemystatus");
                let hasBeenHit = $(i).data("hasbeenhit");

                let locationH = $(i).data("loch");
                let locationV = $(i).data("locv");

                let keyName = locationH+"_"+locationV;
                fieldList.push({ "keyName": keyName, "hasVirus": hasVirus, "hasBeenHit": enemyStatus, "enemyStatus": hasBeenHit });
                //}
            });

            return fieldList;
        },
        clearBoardFromHover: function() {
            //TODO: add later check for fields which ar just actually set

            var eachItem, itemStatus, hasVirus, hasBeenHit;

            for(var i = 0; i < 100; i++) {

                eachItem = $($(".game-field-item")[i]);
                hasVirus = eachItem.data("hasvirus");
                itemStatus = eachItem.data("enemystatus");
                hasBeenHit = eachItem.data("hasbeenhit");

                if(board.gameData.board.fieldMode == "attack") {

                    if(itemStatus == 0) {
                        eachItem.css("background",  board.gameData.properties.boardColour);
                    }

                    if(itemStatus == 1) {
                        eachItem.css("background",  board.gameData.properties.fieldEmpty);
                    }

                    if(itemStatus == 2) {
                        eachItem.css("background",  board.gameData.properties.fieldAttacked);
                    }

                    if(itemStatus == 3) {
                        eachItem.css("background",  board.gameData.properties.fieldSinked);
                    }

                } else {
                    if (hasVirus == 0) {
                        eachItem.css("background", board.gameData.properties.boardColour);
                    } else {
                        eachItem.css("background", board.gameData.properties.virusColour);
                    }

                    if( hasBeenHit == 1) {
                        eachItem.css("background", board.gameData.properties.fieldHasBeenSeenByEnemy);
                    }

                    if(hasBeenHit == 1  && hasVirus == 1) {
                        eachItem.css("background", board.gameData.properties.fieldHasBeenHitByEnemy);
                    }
                }

            }

        },
        confirmBoardForGame: function() {

            //validate is fields ready to be played
            if(!board.boardApi.validateBoardForStart()) {
                //alert("Sorry, but you haven't placed all your items!");
                board.notificationApi.showInGameNotification("Nope, put all viruses!", "negative");
                return;
            }

            if(confirm("Are you ready to play?")) {
                console.log("Change field mode to READY");
                board.gameData.board.fieldMode = "ready";

                console.log("Calling function playerReady()");
                board.socketApi.playerReady();
            }
            //call
            return;
        },
        giveUp: function() {
          if(confirm("Are you sure you want to give up?")) {
              board.socketApi.emitGiveUp();
          }

          return;
        },
        validateBoardForStart: function() {

            var canStart = true;

            if(!board.virusApi.checkAvailableVirus(2)) {
                canStart = false;
            }
            if(!board.virusApi.checkAvailableVirus(3)) {
                canStart = false;
            }
            if(!board.virusApi.checkAvailableVirus(4)) {
                canStart = false;
            }
            if(!board.virusApi.checkAvailableVirus(5)) {
                canStart = false;
            }

            return canStart;
        },
        convertBoardAttackMode: function () {

            //Enemy status
            //0 field not attacked
            //1 field attacked but no enemy
            //2 field attacked enemy found
            //3 field empty because user killed enemy virus

            board.gameData.board.fieldMode = "attack";
            board.boardApi.clearBoardFromHover();

            board.socketApi.changePlayerState(1);
            board.boardApi.startAttackTimer();

            return;
            //when move is ended , move back to defence mode
            //return board.boardApi.convertBoardDefenceMode();
        },
        convertBoardDefenceMode: function () {

            board.gameData.board.fieldMode = "defence";
            board.boardApi.clearBoardFromHover();

            board.socketApi.changePlayerState(2);

            return;
        },
        showMessage: function(title, type, html) {

            clearTimeout(board.gameData.virus.selectedVirusTimeoutId);

            var messageBoard = $("#message-board-bottom");
            var messageBody =  $("#message-board-bottom_body");


            messageBody.find(".row").html("").append(html);
            //remove hidden classes
            messageBoard.addClass(type);
            messageBoard.removeClass("message-board-hidden");

            board.gameData.virus.selectedVirusTimeoutId = setTimeout(function() {
                messageBoard.addClass("message-board-hidden");
            }, 3000);
        },
        markFieldHit: function(locH, locV, toWhat) {
            let item = $("#game-field-item-v"+locV+"-h"+locH);
            item.data("enemystatus", toWhat);
        },
        makeFieldMarkedByEnemy: function(locH, locV) {
            let item = $("#game-field-item-v"+locV+"-h"+locH);
            item.data("hasbeenhit", 1);
        },
        autoFillBoard: function(fieldList, state) {
            $.each(fieldList, function(k, v){
                //console.log(k, v);
                //$.each(v[k], function(j, l) {
                let splitLoc = v["keyName"].split("_");
                console.log(splitLoc);
                let locH = splitLoc[0];
                let locV = splitLoc[1];

                let field = $("#game-field-item-v"+locV+"-h"+locH);

                field.data("hasvirus", v["hasVirus"]);
                field.data("enemystatus",v["enemyStatus"]);
                field.data("hasbeenhit",v["hasBeenHit"]);
              //  });


            });

            if(state == 0) {
                board.gameData.board.fieldMode = "placement";
            }

            if(state == 1) {
                board.gameData.board.fieldMode = "attack";
            }

            if(state == 2) {
                board.gameData.board.fieldMode = "defence";
            }

            board.socketApi.changePlayerState(state);
            board.boardApi.clearBoardFromHover();
        },
        startAttackTimer: function() {
            let timerHolder = $("#card-timer");
            timerHolder.attr("data-timer", board.gameData.board.attackTimer);
            timerHolder.attr("data-status", board.gameData.board.attackTimer)

            timerHolder.addClass("show");
            console.log("does someone starts me?");

            board.boardApi.moveAttackTimer();
            //setTimeout(board.boardApi.moveAttackTimer, 1000);
        },
        moveAttackTimer: function() {
            console.log("But why do i not move?");
            let timeHolder = $("#card-timer");
            let currentTime = timeHolder.attr("data-timer");
            let newTime = currentTime - 1;
            //update time
            console.log(newTime);
            timeHolder.attr("data-timer", newTime);

            if (newTime <= 30 && newTime > 20) {
                timeHolder.attr("data-status", 10)
            }

            if (newTime <= 20 && newTime > 10) {
                timeHolder.attr("data-status", 5)
            }

            if (newTime <= 10) {
                timeHolder.attr("data-status", 3)
            }

            if (newTime >= 0 ) {
                let newWidth = (100 / board.gameData.board.attackTimer) * newTime;
                timeHolder.css("width", newWidth+"%");
                setTimeout(board.boardApi.moveAttackTimer, 500);
            }
        },
        hideAttackTimer: function() {
            let timerHolder = $("#card-timer");
            timerHolder.removeClass("show");
        }
        //make board animations for example when virus is dead or injured
        //follow virus placement on board
        //do other interactions with board
    }


};





$(document).ready(function() {

    var boardController = board;
    boardController.init(234);

    boardController.socket = socket;
    boardController.self = boardController;

    /* SOCKETS */
    console.log("[requestGameData] with data",  {'playerId': myUserId, 'roomId': roomId});
    socket.emit("requestGameData", {'playerId': myUserId, 'roomId': roomId, 'roomName': roomName});

    socket.on("retrieveGameData", function(data){
       console.log("[retrieveGameData] responded with", data);
       boardController.boardApi.autoFillBoard(data["fieldList"], data["gameStatus"]);
    });

    socket.on("bothPlayersReady", function(data) {

        //                    <button class="btn btn-warning btn-start-game visible" id="start-game-button">Start game</button>
        //                     <button class="btn btn-warning btn-warning invisible" id="give-up-game-button">Give up!</button>
        //

        let startButton = $("#start-game-button");
        let giveUpButton = $("#give-up-game-button");

        startButton.removeClass("visible");
        startButton.addClass("invisible");

        giveUpButton.removeClass("invisible");
        giveUpButton.addClass("visible");

        console.log("[bothPlayersReady] ",data);
        if(data["gameStarts"] != myUserId) {
            console.log("converting field to defence");
            boardController.boardApi.convertBoardDefenceMode();
        } else {
            console.log("converting field to attack");
            boardController.boardApi.convertBoardAttackMode();
        }
    });

    socket.on("missedVirus", function(data) {
        boardController.boardApi.markFieldHit(data["locH"], data["locV"], 1);
    });

    socket.on("changeToAttack", function(data){
        //{locationH : locH, locationV: locV, hitTarget: hit}
        if(data["hitTarget"] == 1) {
            //draw hit
            //alert("You got hit!");
            boardController.notificationApi.showInGameNotification("Arggg, he hit you!", "negative");
        } else {
            //draw miss
            //alert("It was a miss");
            boardController.notificationApi.showInGameNotification("Pffff, it was a miss!", "positive");
        }

        boardController.boardApi.makeFieldMarkedByEnemy(data["locationH"], data["locationV"]);
        boardController.boardApi.convertBoardAttackMode();
    });

    socket.on("changeToDefence", function(data){

        if(data["hitTarget"] == 1) {
            //alert("Congrats you got into target");
            //boardController.boardApi.markFieldHit(data["locationH"], data["locationV"], 2);
        } else {
            //alert("Sorry, this field is empty");
            //boardController.boardApi.markFieldHit(data["locationH"], data["locationV"], 1);
        }

        boardController.boardApi.convertBoardDefenceMode();
    });

    socket.on("changeToAttackTimedOut", function(data) {
        boardController.notificationApi.showInGameNotification("Enemy did not make move in time!", "positive");
        boardController.boardApi.convertBoardAttackMode();
    });

    socket.on("changeToDefenceTimedOut", function(data) {
        boardController.notificationApi.showInGameNotification("TIMEOUT! Be quicker next time", "negative");
        boardController.boardApi.convertBoardDefenceMode();
    });

    socket.on("markFieldsAsHit", function(data){

        if(data["type"] == 2) {
            boardController.notificationApi.showInGameNotification("YOU HIT THE TARGET!", "positive");
            //alert("Congrats you got into target");
        } else {
            boardController.notificationApi.showInGameNotification("SORRY, NO HIT!", "negative");
            //alert("Sorry, this field is empty");
        }
        boardController.boardApi.markFieldHit(data["locationH"], data["locationV"], data["type"]);
    });

    socket.on("markFieldAsDestroyed", function(data){
        console.log(data);
        boardController.boardApi.markFieldHit(data["locationH"], data["locationV"], 3);
    });

    socket.on("gameEnd", function(data) {
        let resultHolder = $("#result-holder");

        let message = "";
        let classToAdd = "lost";

        if(data.result == "win") {
            classToAdd = "victory";
            message = "You WON!";
        }

        if(data.result == "lost") {
            classToAdd = "lost";
            message = "You LOST!";
        }

        resultHolder.find("h4").html(message);
        resultHolder.addClass(classToAdd);

        setTimeout(function() {
            window.location.replace("/game/status/"+roomName);
        },3000);
    });

    socket.on("shipIsDead", function(data){
        console.log(data);
        boardController.notificationApi.changeEnemyVirusCount()
    });


    socket.on("waitingForPlayerToStart", function(data) {
        boardController.notificationApi.showInGameNotification(data["message"], "positive");
    });

    /* END SOCKETS */

    $(".btn-virus-selector").on("click", function(el) {
        el.preventDefault();
        boardController.virusApi.selectVirus($(this))
    });

    $(".game-field-item").mouseover(function () {
        //console.log("touched me");
        var locH = $(this).data('loch');
        var locV = $(this).data('locv');

        if(boardController.gameData.board.fieldMode == "defence") {
            boardController.boardApi.clearBoardFromHover();
            return;
        }

        if(boardController.gameData.virus.selectedVirusType != 0 || boardController.gameData.board.fieldMode != "placement") {

            //clean previous drawing
            boardController.boardApi.clearBoardFromHover();

            //get current locs
            if(boardController.gameData.board.fieldMode == "attack") {
                boardController.virusApi.moveAttacker(locH, locV);
            } else {
                boardController.virusApi.moveVirus(locH, locV);
            }

        }

    });

    $(".game-field-item").on("click", function() {
       //check if there is already placed virus, so it would open menu instead of placing one
        var locH = $(this).data('loch');
        var locV = $(this).data('locv');

        if(boardController.gameData.virus.selectedVirusType !=0 || boardController.gameData.board.fieldMode != "placement") {


            if(boardController.gameData.board.fieldMode == "attack") {
                boardController.virusApi.attackVirus(locH, locV);
            } else if(boardController.gameData.board.fieldMode == "placement") {
                boardController.virusApi.placeVirus(locH, locV);
            }
        }

    });

    $("#attack-button").on("click", function() {
        boardController.boardApi.convertBoardAttackMode();
    });

    $("#defence-button").on("click", function() {
        boardController.boardApi.convertBoardDefenceMode();
    });

    $("#start-game-button").on("click", function() {
            boardController.boardApi.confirmBoardForGame();
    });

    $("#give-up-game-button").on("click", function() {
        boardController.boardApi.giveUp();
    });

    $("body").keypress(function(e){

        let keyPressed = (String.fromCharCode(e.charCode)).toLowerCase();

        if(keyPressed == "r") {
            boardController.gameData.virus.rotation = !boardController.gameData.virus.rotation;
        }
    });

});



