<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_rooms', function (Blueprint $table) {
            $table->bigIncrements('game_room_id');
            $table->unsignedInteger("owner_id");
            $table->unsignedInteger("joined_id")->nullable();
            //open - people can join | connected - users are connected | ready - users are playing | complete - game has ended
            $table->string("room_status")->default("open");
            $table->dateTime("room_created_date")->nullable(); //time room was created //if status in 1 h isn't ready it will be closed
            $table->dateTime("room_started_date")->nullable(); //time when game actually started and closed if users hasn't completed in 1 h
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_rooms');
    }
}
