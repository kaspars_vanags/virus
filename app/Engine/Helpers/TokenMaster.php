<?php
/**
 * Created by PhpStorm.
 * User: Kaspars
 * Date: 9/7/2019
 * Time: 14:50
 */

namespace App\Engine\Helpers;


use App\TempAccessTokens;

class TokenMaster
{

    public static function storeAccessToken($accessToken, $expiresOn) {

        $token = new TempAccessTokens();
        $token->access_token = $accessToken;
        $token->expires_on = $expiresOn;

        $token->save();

        return 1;
    }

    public static function getAccessToken() {

        $alternatedKey = sha1(env("GAME_STATIC_CODEX").env("GAME_KEY").env("GAME_ID"));
        $gameKey = env('GAME_KEY');

        $ch = curl_init();

        curl_setopt_array($ch, array(
            CURLOPT_URL => env("GAME_API")."api/token",
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "game-token: {$gameKey}",
                "alternated-key: {$alternatedKey}"
            ),
            CURLOPT_POST => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30
        ));

        $response = curl_exec($ch);
        //$errors = curl_error($ch);

        $r = json_decode($response,true);
       // dd($r);

        self::storeAccessToken($r["accessKey"], $r["validUntil"]);
        //TODO: should check access token and add fallback if token doesn't arrive

        return $r["accessKey"];
    }

}
