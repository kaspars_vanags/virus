<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameRooms extends Model
{
    protected $table = "game_rooms";

    protected $primaryKey = "game_room_id";

    protected $fillable = [
        "owner_id", "joined_id", "room_status", "room_created_date", "room_started_date", "game_type", "unique_room_name", "winner_id"
    ];

}
