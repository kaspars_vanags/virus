<?php

namespace App\Http\Controllers\Auth;

use App\GameRooms;
use Carbon\Carbon;
use Faker\Provider\DateTime;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Auth;

class GameController extends Controller
{

    public function indexAction() {
        //list of games user can play
        $gamesAvailable = $this->findGamesInProgress();
        //search game in progress
        //this is main game page, where ytou can select type of game
        return view("auth.game")->with("gamesAvailable", $gamesAvailable);
    }

    public function gameStatusPage($roomName) {
        $gameRoom = DB::table("game_rooms")->where("unique_room_name", "=", $roomName)->first();
        $playerData = [
            "winnerId" => "",
            "winnerName" => "",
            "looserId" => "",
            "looserName" => "",
            "timePlayed" => ""
        ];

        if(!$gameRoom) {
            return redirect()->back();
        }

        $playerData["winnerId"] = $gameRoom->winner_id;
        $playerData["looserId"] = ($gameRoom->winner_id == $gameRoom->owner_id) ? $gameRoom->joined_id : $gameRoom->owner_id;
        /*
        if(!is_null($gameRoom->winner_id)) {
            $status = (Auth::user()->user_id == $gameRoom->winner_id) ? "victory" : "lost";
        }
        */

        $playerData["winnerName"] = (DB::table("users")->select("username")->where("user_id", $playerData["winnerId"])->first())->username;
        $playerData["looserName"] = (DB::table("users")->select("username")->where("user_id", $playerData["looserId"])->first())->username;
       /*
        if($gameRoom->owner_id == Auth::user()->user_id) {
            $enemy =
        }

        if($gameRoom->joined_id == Auth::user()->user_id) {
            $enemy = DB::table("users")->select("username")->where("user_id", $gameRoom->owner_id)->first();
        }
       */

        $dateStarted = Carbon::create($gameRoom->room_started_date);
        if(is_null($gameRoom->updated_at)) {
            $dateEnd = Carbon::now();
        } else {
            $dateEnd = Carbon::create($gameRoom->updated_at);
        }

        $playerData["timePlayed"] = $dateStarted->diffInMinutes($dateEnd);
     //   $diffPlayed =

/*
        $gameInfo = [
            "enemyName" => $enemy->username,
            "status" => $status,
            "timePlayed" =>$diffPlayed,
        ];*/

        return view("auth.games.info")->with("info", $playerData);
    }

    public function oneVsOneAction() {
        $data = [
            "verticalNaming" => array("UM", "BM", "TP", "IC", "SK", "DC", "ER", "OF", "PE", "MC")
        ];
        return view("auth.games.1vs1")->with("data", $data);
    }

    public function oneVsPcAction() {

    }

    public function twoVsTwoAction() {

    }



    public function createGameRoom(Request $request) {

        $gameType = $request->get("type");
        $userId = $request->get("userid");

        if($gameType == 'onevsone') {
            $gameType = 1;
        }

        if($gameType == 'onevscom') {
            $gameType = 2;
        }

        if($gameType == 'twovstwo') {
            $gameType = 3;
        }

        $currentDate = new \DateTime();
      //  dd( Auth::user()->user_id);
        //create unique room name

        $uniqName = md5( chr(rand(65, 90)).chr(rand(65, 90)).chr(rand(65, 90)).chr(rand(65, 90)) );

        //1 serach for game in this type
        $gameRoomID = DB::table("game_rooms")->insertGetId([
            "owner_id" => $userId,
            "room_created_date" => $currentDate,
            "room_started_date" => $currentDate,
            "game_type" => $gameType,
            "unique_room_name" => $uniqName
        ]);

        return new JsonResponse(["roomId" => $gameRoomID, "gameType" => $gameType, "roomName" => $uniqName, "status" => 1], 200);

        //2 if no empty room exist connect to the room

        //return room ID and responmse like connect or wait


    }

    public function closeGameRoom(Request $request) {
        $roomId = $request->get("roomId");
        DB::table("game_rooms")->where("game_room_id", "=", $roomId)->update(["room_status" => "complete"]);
        return new JsonResponse(["status" => 1], 200);
    }

    public function actualGameRoom(Request $request, $roomId, $gameName) {
        //get room info
        $gameRoom = DB::table("game_rooms")->where("game_room_id", "=", $roomId)->first();
//dd($gameRoom);
        if(is_null($gameRoom->joined_id) && $gameRoom->owner_id != Auth::user()->user_id) {
            GameRooms::where("game_room_id", $roomId)->update(["joined_id" => Auth::user()->user_id]);
        }

        $gameRoomInfo = [
            "ownerId" => $gameRoom->owner_id,
            "joinedId" => $gameRoom->joined_id,
            "roomStatus" => $gameRoom->room_status,
            "gameType" => $gameRoom->game_type,
            "roomName" => $gameRoom->unique_room_name,
            "roomId" => $gameRoom->game_room_id
        ];

        //why i need it?
        $data = [
            "verticalNaming" => array("UM", "BM", "TP", "IC", "SK", "DC", "ER", "OF", "PE", "MC")
        ];

        return view("auth.games.gameroom")->with(["response" => $gameRoomInfo, "data" => $data]);
    }

    public function gameInfo($gameName, $status) {
        $gameRoom = DB::table("game_rooms")->where("unique_room_name", "=", $gameName)->first();

        if(!$gameRoom) {
            return redirect()->back();
        }

        if(!is_null($gameRoom->winner_id)) {
            $status = (Auth::user()->user_id == $gameRoom->winner_id) ? "victory" : "lost";
        }

        if($gameRoom->owner_id == Auth::user()->user_id) {
            $enemy = DB::table("users")->select("username")->where("user_id", $gameRoom->joined_id)->first();
        }

        if($gameRoom->joined_id == Auth::user()->user_id) {
            $enemy = DB::table("users")->select("username")->where("user_id", $gameRoom->owner_id)->first();
        }

        $dateStarted = Carbon::create($gameRoom->room_started_date);
        if(is_null($gameRoom->updated_at)) {
            $dateEnd = Carbon::now();
        } else {
            $dateEnd = Carbon::create($gameRoom->updated_at);
        }

        $diffPlayed = $dateStarted->diffInMinutes($dateEnd);


        $gameInfo = [
            "enemyName" => $enemy->username,
            "status" => $status,
            "timePlayed" =>$diffPlayed,
        ];

        return view("auth.games.info")->with("info", $gameInfo);

    }

    private function findGamesInProgress() {
        //1 - 1vs1 | 2 - 1 vs PC | 3 - 2 vs 2
        $gameType = [
            "1vs1" => 0,
            "1vsPc" => 0,
            "2vs2" => 0
        ]; //"owner_id", "joined_id", "room_status", "room_created_date", "room_started_date", "game_type"

        $games = DB::table("game_rooms")
            ->where("owner_id",  "=", Auth::user()->user_id)
            ->where("room_status", "!=", "complete")
            ->get();

        foreach($games as $k => $v) {
            if($v->game_type == 1) {
                $gameType['1vs1'] = $v->game_room_id;
             //   $gameType['1vs1'] = ["roomId" => $v['game_room_id'], "roomStatus" => $v['room_status'], "opponentId" => $v['joined_id']];
                continue;
            }

            if($v->game_type == 2) {
                $gameType['1vsPc'] = $v->game_room_id;
          //      $gameType['1vsPc'] = ["roomId" => $v['game_room_id'], "roomStatus" => $v['room_status'], "opponentId" => $v['joined_id']];
                continue;
            }

            if($v->game_type == 3) {
                $gameType['2vs2'] = $v->game_room_id;
            //    $gameType['2vs2'] = ["roomId" => $v['game_room_id'], "roomStatus" => $v['room_status'], "opponentId" => $v['joined_id']];
                continue;
            }
        }
        return $gameType;
    }

}
