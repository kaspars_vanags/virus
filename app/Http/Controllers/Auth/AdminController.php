<?php

namespace App\Http\Controllers\Auth;

use App\GameRooms;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{

    public function indexAction()
    {
        $response = [
            'rooms' => [],
        ];

        $response['rooms'] = GameRooms::orderBy("game_room_id", "desc")->get();
        return view("auth.admin.dashboard")->with("response", $response);
    }

    public function nodeAdminController()
    {
        $roomList = GameRooms::orderBy("game_room_id", "desc")->get();
        return view("auth.admin.node")->with("roomList", $roomList);
    }

    public function closeRoom($roomId)
    {
        //DB::
        DB::table("game_rooms")->where("game_room_id", "=", $roomId)->update(["room_status" => "complete"]);
     //   GameRooms::where("game_room_id", "=", $roomId)->update();
        return redirect("/admin/dashboard");//->back();
    }

}
