<?php

namespace App\Http\Controllers\Auth;

use App\GameRooms;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{

    public function indexAction() {

        //get game history
        $data = [];

        //$gameHistory = GameRooms::where("owner_id", Auth::user()->user_id)->get();
        $gameHistory = DB::table("game_rooms")
            ->whereRaw("(owner_id = ? OR joined_id = ?) AND joined_id IS NOT NULL", [Auth::user()->user_id, Auth::user()->user_id])
            ->orderBy('game_room_id', 'desc')
            ->limit(20)
            ->get();

        $gameData = [];

        /*
         *       +"game_room_id": 4
      +"owner_id": 1
      +"joined_id": null
      +"room_status": "complete"
      +"room_created_date": "2020-04-06 14:36:07"
      +"room_started_date": "2020-04-06 14:36:07"
      +"game_type": 1
      +"unique_room_name": "92c1430935c8c6b9680d2c195591596d"
         */

        foreach ($gameHistory as $gm) {

            $enemy = "Unknown";

            if($gm->owner_id == Auth::user()->user_id) {
                $enemy = DB::table("users")->select("username")->where("user_id", $gm->joined_id)->first();
            }

            if($gm->joined_id == Auth::user()->user_id) {
                $enemy = DB::table("users")->select("username")->where("user_id", $gm->owner_id)->first();
            }

            $dateWhen = new Carbon($gm->room_started_date);

            $winner = false;
            if(!is_null($gm->winner_id) && $gm->winner_id == Auth::user()->user_id) {
                $winner = true;
            }

            $gameData[$gm->game_room_id] = ["status" => $gm->room_status, "enemy" => $enemy->username, "when" => $dateWhen->format("d M"), "roomName" => $gm->unique_room_name, "winner" => $winner];

        }


        $data["game_history"] = $gameData;
        return view("auth.profile")->with("data", $data);
    }
}
