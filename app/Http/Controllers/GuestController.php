<?php

namespace App\Http\Controllers;

use App\Engine\Helpers\TokenMaster;
use App\GameRooms;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class GuestController extends Controller
{


    public function indexAction() {
        //leaderboard
        $leaderBoard = $this->getLeaderBoard();
        return view("guest.home")->with("leaderBoard", $leaderBoard);
    }

    public function loginAction() {
        return view("guest.login");
    }

    public function loginPostAction(Request $request) {
        $req = $request->all();

        if(Auth::attempt(["username" => $req["username"], "password" => $req["password"]], true)) {
            return redirect("/profile");
        }

        return redirect("/login");
        //TODO: add auth logic
    }

    public function logout() {
        Auth::logout();
        return redirect("/");
    }

    public function registerPostAction(Request $request) {

        $req = $request->all();

        //get access token from main server
        $accessToken = TokenMaster::getAccessToken();//$this->getAccessToken();

        //TODO:Validate users data

        //check if user is already registered (on ui);
        $userExist = DB::table("users")->where(["email" => $req["reg-email"]])->first();

        if($userExist) {
            return new JsonResponse(["status" => 0, "message" => "User already exist", "alertLevel" => "1"], 200);
        }

        //register user at our end
        $userId = DB::table("users")->insertGetId([
            "username" => $req["reg-username"],
            "email" => $req["reg-email"],
            "password" => bcrypt($req["reg-password"]),
            "master_id" => 0
        ]);

        //update user with access token
        $externalUserData = $this->registerRemoteUser($req, $accessToken);

        //TODO:Validate api response

        //update user with external details
        try {
            DB::table("users")->where(["user_id" => $userId])->update([
                "master_id" => $externalUserData["uid"],
                "game_access_token" => $externalUserData["game_access_token"]
            ]);
        } catch (\Exception $e) {
            return redirect("/");
        }

        if(Auth::attempt(["username" => $req["reg-username"], "password" => $req["reg-password"]]) ) {
            return redirect("/profile");
        }

        //TODO: auth user if successful, else thor some error message
        //Get laracast flashbag
        return redirect("/");

    }

    protected function registerRemoteUser($req, $accessToken) {

        //prepare data
        $alternatedKey = sha1(env("GAME_STATIC_CODEX").env("GAME_KEY").env("GAME_ID"));
        $gameKey = env('GAME_KEY');

        //post data
        $postData = [
            "name" => $req["reg-username"],
            "email" => $req["reg-email"],
            "password" => $req["reg-password"],
            "token" => $accessToken
        ];


        $ch = curl_init();

        curl_setopt_array($ch, array(
            CURLOPT_URL => env("GAME_API")."api/user/register",
            CURLOPT_HTTPHEADER => array(
                "game-token: {$gameKey}",
                "alternated-key: {$alternatedKey}"
            ),
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30
        ));

        $response = curl_exec($ch);
        $errors = curl_error($ch);

        $r = json_decode($response, true);
        return $r;
    }

    protected function getLeaderBoard() {

        $all1vs1CompletedGames = GameRooms::where("winner_id", "!=", null)->where("game_type", "=", 1)->get();
        $all1vscomCompletedGames = GameRooms::where("winner_id", "!=", null)->where("game_type", "=", 2)->get();
        $all2vs2CompletedGames = GameRooms::where("winner_id", "!=", null)->where("game_type", "=", 3)->get();

        $leaderBoardList = [
            "1vs1" => $this->getWinnerList($all1vs1CompletedGames),
            "1vscom" => $this->getWinnerList($all1vscomCompletedGames),
            "2vs2" => $this->getWinnerList($all2vs2CompletedGames),
        ];

        return $leaderBoardList;
    }

    private function getWinnerList($allGames) {
        $victoryList = [];

        foreach ($allGames as $game) {
            if(key_exists($game["winner_id"], $victoryList)) {
                $victoryList[$game["winner_id"]]["wins"]+=1;
            } else {
                $victoryList[$game["winner_id"]]["wins"] = 1;
            }
        }

        foreach ($victoryList as $key => $item) {
            $user = User::where("user_id", "=", $key)->first();
            $victoryList[$key]["username"] = $user["username"];
        }

        return $victoryList;
    }

}
