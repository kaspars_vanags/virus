<?php

namespace App\Http\Controllers\Api;

use App\GameRooms;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GameExternalController extends Controller
{

    const API_KEY =  "iD2MaE7IoMWFwuti9D5lEIXimfl3VSr5";

    public function setWinner($gameName, $winnersId, $apiKey) {
        if($apiKey != self::API_KEY) {
            return response()->json(["status" => 0, "message" => "nope, not happening!"], 503);
        }

        GameRooms::where("unique_room_name", $gameName)->update(["winner_id" => $winnersId, "room_status" => "complete"]);
        return response()->json(["status" => 1, "message" => "Well, something did happen"], 200);
    }

}
