<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempAccessTokens extends Model
{

    public $timestamps = false;
    protected $table = "temp_access_tokens";


    protected $fillable = [
        "access_token", "expires_on"
    ];


}
