<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//save winner
Route::get("/game/complete/{gameName}/{winnersId}/{apiKey}", "Api\GameExternalController@setWinner");
Route::get("/game/create/gameroom", "Auth\GameController@createGameRoom");
Route::get("/game/close/gameroom", "Auth\GameController@closeGameRoom");

Route::group(["middleware" => "guest"],function(){

    //open landing page
    Route::get('/', 'GuestController@indexAction');

    //authentication and registration page
    Route::get('/login', 'GuestController@loginAction')->name("login");

    //complete login
    Route::post('/login', 'GuestController@loginPostAction');
    //complete registration
    Route::post('/register', 'GuestController@registerPostAction');



});

Route::group(["middleware" => "auth"], function() {
    //some dashboards
    Route::get("/profile", "Auth\ProfileController@indexAction");

    Route::get("/logout", "GuestController@logout");

    //game choice place
    Route::get("/game", "Auth\GameController@indexAction");

    //game 1 vs 1
   // Route::get("/game/onevssone/{roomId}", "Auth\GameController@oneVsOneAction");

    //game 1 vs com
 //   Route::get("/game/onevscom", "Auth\GameController@oneVsPcAction");

    //game 2 vs 2
 //   Route::get("/game/twovstwo", "Auth\GameController@twoVsTwoAction");

    //actual game room
    Route::get("/game/active/{gameName}/{roomId}", "Auth\GameController@actualGameRoom");

    Route::get("/game/status/{roomName}", "Auth\GameController@gameStatusPage");
    //game itself
    //announcement room and stats
    //Route::get("/game/finished/{gameName}/{status}", "Auth\GameController@gameInfo");

    //admin temporary stuff
    Route::get("/admin/dashboard", "Auth\AdminController@indexAction");
    Route::get("/admin/node", "Auth\AdminController@nodeAdminController");


    Route::get("/admin/room/close/{id}", "Auth\AdminController@closeRoom");
    //other stuff
});
